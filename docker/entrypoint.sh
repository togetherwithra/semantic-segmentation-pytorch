#!/bin/sh

cd /home/semantic_segmentation/application
pip3 install -r ./requirements.txt

touch /var/log.txt
chmod a+rw /var/log.txt

exec su semantic_segmentation -c 'gunicorn app:app -b 0.0.0.0:5108 -w 1  --log-file=- >> /var/log.txt 2>&1'
